﻿using Newtonsoft.Json;
using Sol_Session_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Session_Login
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "" && txtPassword.Text == "")
            {
                {
                    lblMessage.Text = "User and Password should not be null";
                }
            }
            else
            {
                UserEntity userEntityObj = new UserEntity()
                {
                    Login = new LoginEntity()
                    {
                        UserName = txtUserName.Text,
                        Password = txtPassword.Text
                    }
                };

                // Serialize User Data into Json
                string userJsonData = JsonConvert.SerializeObject(userEntityObj);

                // Store student Json Value in Session
                Session["UserInfo"] = userJsonData;

                // Redirect to WebForm 2
                Response.Redirect("~/User.aspx");
            }
            }

        }
    }