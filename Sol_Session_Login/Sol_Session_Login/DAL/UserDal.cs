﻿using Sol_Session_Login.DAL.ORD;
using Sol_Session_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Session_Login.DAL
{
    public class UserDal
    {
        #region Declaration
        private UserDcDataContext dbObj = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dbObj = new UserDcDataContext();
        }
        #endregion

        #region Public Method
        public dynamic GetUserData(UserEntity entityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                return
                 dbObj
                 ?.uspGetUser
                 (
                     "Login",
                     entityObj?.UserId,
                     entityObj?.FirstName,
                     entityObj?.LastName,
                     entityObj?.Login?.UserName,
                     entityObj?.Login?.Password,
                     ref status,
                     ref message
                 )
                 .AsEnumerable()
                 .Select((lelinqUserObj) => new UserEntity()
                 {
                     UserId = lelinqUserObj.UserID,
                     FirstName = lelinqUserObj.FirstName,
                     LastName = lelinqUserObj.LastName,
                     Login = new LoginEntity()
                     {
                         UserName = lelinqUserObj.UserName,
                         Password = lelinqUserObj.Password
                     }

                 }
                 )
                 ?.FirstOrDefault();
            }

            catch(Exception)
            {
                throw;
            }
          }

        #endregion

    }
}