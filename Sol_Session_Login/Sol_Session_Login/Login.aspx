﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Sol_Session_Login.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">

       table
        {
            width : 50%;
            margin:auto;
            border-collapse:collapse;
        }
       .textBox
       {
            border:2px;
            border-color:black;
            border-style:solid;
            border-radius:5px;
            width:50%;
            height:30px;
            padding:2px;

       }

       #btnSubmit
       {
           background-color:cornflowerblue;
           color:rebeccapurple;
       }

    </style>

</head>

    
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" placeHolder="UserName" CssClass="textBox"></asp:TextBox>
                 </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" CssClass="textBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Login"  OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
