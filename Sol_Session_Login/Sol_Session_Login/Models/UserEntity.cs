﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Session_Login.Models
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public LoginEntity Login { get; set; }

    }
}