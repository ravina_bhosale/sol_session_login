﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_Session_Login.Models
{
    public class LoginEntity
    {
        public decimal? UserId { get; set; }
        
        public string UserName { get; set; }
        
        public string Password { get; set; } 
    }
}