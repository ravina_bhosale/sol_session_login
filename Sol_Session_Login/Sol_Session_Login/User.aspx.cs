﻿using Newtonsoft.Json;
using Sol_Session_Login.DAL;
using Sol_Session_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Session_Login
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserInfo"] != null)
            {
                // read Session Value
                string userJsonData = Session["UserInfo"].ToString();

                // DeSerialize User Json into  Object
                UserEntity userEntitytObj = JsonConvert.DeserializeObject<UserEntity>(userJsonData);

                // Bind  Data into Lable Controls.
                lblFullName.Text= new StringBuilder()
                                                    .Append("Welcome ")
                                                    .Append(userEntitytObj.FirstName)
                                                    .Append(" ")
                                                    .Append(userEntitytObj.LastName)
                                                    .ToString();
                //lblLastName.Text = userEntitytObj.LastName;

                // Get Session Id
                //Response.Write("Session Id :" + Session.SessionID);
                //Response.Write("<br/>");

                //Read Session Id using Cookie
               //HttpCookie httpCookieObj = Request.Cookies["ASP.NET_SessionId"];

               // Response.Write("Session Id Using Cookie : " + httpCookieObj.Value);
            }
            else
            {
                Response.Redirect("~/Login.aspx",false);
            }
        }


         protected void btnLogout_Click(object sender, EventArgs e)
        {
            // Remove Session Value first.
            Session.Remove("UserInfo");

            // Completely Destroy current Session Only
            Session.Abandon();

            // Rediret to Defualt Page
            Response.Redirect("~/Login.aspx");

        }
    }
}