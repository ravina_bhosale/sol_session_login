﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sol_Session_Login.DAL;
using Sol_Session_Login.Models;

namespace UTP
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void UserTestMethod()
        {
            

            var userEntityObj = new UserEntity()
            {
                Login = new LoginEntity()
                {
                    UserName = "ravina",
                    Password = "1234"
                }
            };

            var result = new UserDal()?.GetUserData(userEntityObj);

            Assert.IsNotNull(result);

        }
    }
}

