﻿CREATE TABLE [dbo].[tblUser] (
    [UserID]    NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

