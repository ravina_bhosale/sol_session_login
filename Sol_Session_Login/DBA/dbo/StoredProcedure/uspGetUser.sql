﻿CREATE PROCEDURE [dbo].[uspGetUser]
	
	@Command Varchar(50)=NULL,

	@UserId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@UserName Varchar(50)=NULL,
	@Password Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	AS

	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Login'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY


				IF  exists(select UL.UserName,UL.Password
							 from tblUserLogin as UL 
								where UserName=@UserName and Password=@Password)
						BEGIN
							SET @Status=1
							SET @Message='Login Success'

						SELECT 
							U.FirstName,
							U.LastName
						FROM tblUserLogin AS UL
							INNER JOIN
								tblUser AS U
									ON
										UL.UserID=U.UserID
											WHERE UL.UserName=@UserName and UL.Password=@Password
						END
				ELSE	
						BEGIN
							SET @Status=0
							SET @Message='User name and Password are wrong'
						END

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

			SET @ErrorMessage=ERROR_MESSAGE()
			SET @Status=0
			SET @Message='Stored Proc execution Fail'

			RAISERROR(@ErrorMessage,16,1)
					
			ROLLBACK TRANSACTION

			END CATCH
	END

	END

	GO

